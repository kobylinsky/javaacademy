Java Academy
=========

**Java Academy** is a online Java tutorial, which will improve your Java skills.

  - Sign up for free
  - Choose themes for studying
  - Enjoy learning Java!

Tech
-----------

Java Academy uses a number of frameworks to work properly:

* [Maven 2] - software project management and comprehension tool.
* [Glassfish 4.0] - an open source application server.
* [Hibernate 4.2.1] - an open source Java persistence framework.
* [JSF 2.1] - a Java specification for building component-based user interfaces for web applications.
* [Twitter Bootstrap 3.1.1] - a sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development.
* [JUnit 4.11] - a simple framework to write repeatable tests.
* [EJB 3.2] - managed, server-side component architecture for modular construction of enterprise applications.

Tags
--------------

* *[lab1]* - Made basic DAO for User, Theme, Progress entities using JDBC connection.
* *[lab2]* - Fully implemented support of JSP/Servlet for Users, Themes and Progresses; MongoDB support.
* *[lab3]* - Removed support of JDBC/Mongo and implemented JPA (Hibernate).
* *[lab4]* - Migrated from JSP/Servlets to JSF.
* *[lab5]* - EJB support.
* *[lab6]* - JAAS support: authentication/authorization
* *[lab7]* - JAX-RS web-service using JAXB for mapping entities to XML.


Branches
--------------

* ***[master]***

[Maven 2]:http://maven.apache.org/download.cgi
[Glassfish 4.0]:https://glassfish.java.net/download.html
[Hibernate 4.2.1]:http://planet.jboss.org/post/hibernate_orm_4_2_1_final_and_4_1_12_final_released
[JSF 2.1]:http://mvnrepository.com/artifact/javax.faces/jsf-api/2.1
[JUnit 4.11]:https://github.com/junit-team/junit/wiki/Download-and-Install
[Twitter Bootstrap 3.1.1]:http://twitter.github.com/bootstrap/
[EJB 3.2]:http://en.wikipedia.org/wiki/Enterprise_JavaBeans


[lab1]:https://bitbucket.org/bk92/javaacademy/commits/tag/lab1
[lab2]:https://bitbucket.org/bk92/javaacademy/commits/tag/lab2
[lab3]:https://bitbucket.org/bk92/javaacademy/commits/tag/lab3
[lab4]:https://bitbucket.org/bk92/javaacademy/commits/tag/lab4
[lab5]:https://bitbucket.org/bk92/javaacademy/commits/tag/lab5
[lab6]:https://bitbucket.org/bk92/javaacademy/commits/tag/lab6
[lab7]:https://bitbucket.org/bk92/javaacademy/commits/tag/lab7
[master]:https://bitbucket.org/bk92/javaacademy/branch/master