import com.bogdankobylinsky.javaacademy.dao.DAOFactory;
import com.bogdankobylinsky.javaacademy.dao.SectionDAO;
import com.bogdankobylinsky.javaacademy.dao.ThemeDAO;
import com.bogdankobylinsky.javaacademy.entities.Section;
import com.bogdankobylinsky.javaacademy.entities.Theme;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ThemeTest {

    @Test
    public void test_AddGetDelete() {
        SectionDAO sectionDAO = DAOFactory.getInstance().getSectionDAO();
        Section section = new Section("JAVA");
        sectionDAO.add(section);

        ThemeDAO themeDAO = DAOFactory.getInstance().getThemeDAO();

        List<Theme> themesBeforeManipulations = themeDAO.getAll();

        Theme theme = new Theme();
        theme.setName("Часть 1. Введение в технологии Java");
        theme.setContent("{content}");
        theme.setSection(section);
        themeDAO.add(theme);

        assertEquals(themesBeforeManipulations.size() + 1, themeDAO.getAll().size());
        assertEquals(theme, themeDAO.getByID(theme.getId()));

        themeDAO.delete(theme);
        assertNull(themeDAO.getByID(theme.getId()));
        sectionDAO.delete(section);
        assertNull(sectionDAO.getByID(section.getId()));

        assertEquals(themesBeforeManipulations.size(), themeDAO.getAll().size());

    }

    @Test
    public void test_MassDelete() {
        SectionDAO sectionDAO = DAOFactory.getInstance().getSectionDAO();
        Section section = new Section("JAVA");
        sectionDAO.add(section);

        ThemeDAO themeDAO = DAOFactory.getInstance().getThemeDAO();

        List<Theme> themesBeforeDelete = themeDAO.getAll();

        List<Theme> themes = new ArrayList<Theme>();
        for (int i = 0; i < 10; i++)
            themes.add(new Theme("for delete", "fordelete", section));

        for (Theme theme : themes)
            themeDAO.add(theme);
        for (Theme theme : themes)
            themeDAO.delete(theme);

        List<Theme> themesAfterDelete = themeDAO.getAll();

        assertTrue(themesBeforeDelete.containsAll(themesAfterDelete));
        assertTrue(themesAfterDelete.containsAll(themesBeforeDelete));

        sectionDAO.delete(section);
        assertNull(sectionDAO.getByID(section.getId()));
    }

    @Test
    public void test_AddUpdateDelete() {
        SectionDAO sectionDAO = DAOFactory.getInstance().getSectionDAO();
        Section section = new Section("JAVA");
        sectionDAO.add(section);

        ThemeDAO themeDAO = DAOFactory.getInstance().getThemeDAO();
        Theme theme = new Theme("name1", "content1", section);
        themeDAO.add(theme);

        theme.setName("name2");
        theme.setContent("content2");

        themeDAO.update(theme);

        Assert.assertEquals(theme, themeDAO.getByID(theme.getId()));

        themeDAO.delete(theme);
        assertNull(themeDAO.getByID(theme.getId()));
        sectionDAO.delete(section);
        assertNull(sectionDAO.getByID(section.getId()));
    }
}
