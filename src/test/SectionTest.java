import com.bogdankobylinsky.javaacademy.dao.DAOFactory;
import com.bogdankobylinsky.javaacademy.dao.SectionDAO;
import com.bogdankobylinsky.javaacademy.dao.ThemeDAO;
import com.bogdankobylinsky.javaacademy.entities.Section;
import com.bogdankobylinsky.javaacademy.entities.Theme;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SectionTest {

    @Test
    public void test_AddGetDelete() {
        SectionDAO sectionDAO = DAOFactory.getInstance().getSectionDAO();
        ThemeDAO themeDAO = DAOFactory.getInstance().getThemeDAO();

        List<Section> sectionsBeforeManipulations = sectionDAO.getAll();

        Section section = new Section("JAVA");
        sectionDAO.add(section);
        List<Theme> themes = new ArrayList<Theme>();
        for (int i = 0; i < 10; i++) {
            Theme theme = new Theme("Part" + i, "forDelete", section);
            themes.add(theme);
            themeDAO.add(theme);
            //sectionDAO.update(section);
        }

        assertEquals(sectionsBeforeManipulations.size() + 1, sectionDAO.getAll().size());
        assertEquals(section, sectionDAO.getByID(section.getId()));
        assertTrue(sectionDAO.getByID(section.getId()).getThemes().containsAll(themes));
        assertTrue(themes.containsAll(sectionDAO.getByID(section.getId()).getThemes()));

        for (Theme theme : themes) {
            themeDAO.delete(theme);
        }

        sectionDAO.delete(section);
        assertNull(sectionDAO.getByID(section.getId()));

        assertEquals(sectionsBeforeManipulations.size(), sectionDAO.getAll().size());
    }

    @Test
    public void test_MassDelete() {
        SectionDAO sectionDAO = DAOFactory.getInstance().getSectionDAO();
        ThemeDAO themeDAO = DAOFactory.getInstance().getThemeDAO();

        List<Section> sectionsBeforeManipulations = sectionDAO.getAll();

        List<Section> sections = new ArrayList<Section>();
        for (int i = 0; i < 10; i++) {
            Section section = new Section("NewSection");
            new Theme("for delete", "fordelete", section);
            sections.add(section);
            sectionDAO.add(section);
        }

        for (Section section : sections) {
            for (Theme theme : section.getThemes()) {
                themeDAO.delete(theme);
            }
            sectionDAO.delete(section);
        }

        assertEquals(sectionsBeforeManipulations.size(), sectionDAO.getAll().size());
    }

    @Test
    public void test_AddUpdate() {
        SectionDAO sectionDAO = DAOFactory.getInstance().getSectionDAO();
        ThemeDAO themeDAO = DAOFactory.getInstance().getThemeDAO();

        Section section = new Section("sctn");
        Theme theme = new Theme("name1", "content1", section);
        sectionDAO.add(section);

        section.deleteTheme(theme);
        section.setName("NEWNAME");

        sectionDAO.update(section);

        assertEquals(section, sectionDAO.getByID(section.getId()));
        assertEquals(0, section.getThemes().size());

        themeDAO.delete(theme);
        assertNull(themeDAO.getByID(theme.getId()));
        sectionDAO.delete(section);
        assertNull(sectionDAO.getByID(section.getId()));
    }

    @Test
    public void test_ReassignThemesBeyondSections() {
        SectionDAO sectionDAO = DAOFactory.getInstance().getSectionDAO();
        ThemeDAO themeDAO = DAOFactory.getInstance().getThemeDAO();

        Section section1 = new Section("sctn1");
        Theme theme1 = new Theme("name2", "content2", section1);
        sectionDAO.add(section1);

        Section section2 = new Section("sctn2");
        Theme theme2 = new Theme("name12", "content2", section2);
        sectionDAO.add(section2);

        section1.deleteTheme(theme1);
        section2.deleteTheme(theme2);
        section1.addTheme(theme2);
        section2.addTheme(theme1);

        sectionDAO.update(section1);
        sectionDAO.update(section2);

        assertEquals(section1, sectionDAO.getByID(section1.getId()));
        assertEquals(section2, sectionDAO.getByID(section2.getId()));
        assertEquals(1, section1.getThemes().size());
        assertEquals(1, section2.getThemes().size());
        assertEquals(theme1, section2.getThemes().get(0));
        assertEquals(theme2, section1.getThemes().get(0));

        themeDAO.delete(theme1);
        themeDAO.delete(theme2);
        sectionDAO.delete(section1);
        sectionDAO.delete(section2);
    }

}
