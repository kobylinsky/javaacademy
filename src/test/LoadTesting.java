import com.bogdankobylinsky.javaacademy.dao.DAOFactory;
import com.bogdankobylinsky.javaacademy.entities.Progress;
import com.bogdankobylinsky.javaacademy.entities.Section;
import com.bogdankobylinsky.javaacademy.entities.Theme;
import com.bogdankobylinsky.javaacademy.entities.User;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class LoadTesting {

    @Test
    public void testRetrievingProgresses() {
        int loadFactor = 100;

        List<User> users = new ArrayList<>();
        for (int i = 0; i < loadFactor; i++) {
            User user = new User("Name" + String.valueOf(i), "Surname" + String.valueOf(i),
                    "Login" + String.valueOf(i), "Password" + String.valueOf(i));
            DAOFactory.getInstance().getUserDAO().add(user);
            users.add(user);
        }


        Section section = new Section("SectionNAME");
        DAOFactory.getInstance().getSectionDAO().add(section);
        Theme theme = new Theme("ThemeNAME", "ThemeCONTENT", section);
        DAOFactory.getInstance().getThemeDAO().add(theme);
        for (int i = 0; i < loadFactor; i++) {
            DAOFactory.getInstance().getProgressDAO().add(new Progress(users.get(i), theme, i * 100 / loadFactor));
        }

        long startTime1 = System.currentTimeMillis();
        for (int i = 0; i < 5; i++) {
            List<Progress> progresses1 = DAOFactory.getInstance().getProgressDAO().getProgresses(theme);
        }
        long stopTime1 = System.currentTimeMillis();


        long startTime2 = System.currentTimeMillis();
        for (int i = 0; i < 5; i++) {
            List<Progress> progresses2 = DAOFactory.getInstance().getThemeDAO().getByID(theme.getId()).getProgresses();
        }
        long stopTime2 = System.currentTimeMillis();


        // 1 case ~ 500-700 ms
        System.out.println("Time for retrieving progresses through ProgressDAO: " + (stopTime1 - startTime1) + "ms.");
        // 2 case ~ 250-350 ms
        System.out.println("Time for retrieving progresses through ThemeDAO: " + (stopTime2 - startTime2) + "ms.");


        for (User user : users) DAOFactory.getInstance().getUserDAO().delete(user);
        DAOFactory.getInstance().getThemeDAO().delete(theme);
        DAOFactory.getInstance().getSectionDAO().delete(section);
    }
}
