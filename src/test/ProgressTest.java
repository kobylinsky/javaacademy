import com.bogdankobylinsky.javaacademy.dao.*;
import com.bogdankobylinsky.javaacademy.entities.Progress;
import com.bogdankobylinsky.javaacademy.entities.Section;
import com.bogdankobylinsky.javaacademy.entities.Theme;
import com.bogdankobylinsky.javaacademy.entities.User;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ProgressTest {

    @Test
    public void test_AddGetDelete() {
        SectionDAO sectionDAO = DAOFactory.getInstance().getSectionDAO();
        Section section = new Section("JAVA");
        sectionDAO.add(section);

        ThemeDAO themeDAO = DAOFactory.getInstance().getThemeDAO();
        Theme theme = new Theme("Theme 1", "content content1", section);
        themeDAO.add(theme);

        UserDAO userDAO = DAOFactory.getInstance().getUserDAO();
        User user = new User("Bogdan", "Kobylinsky", "b@k.a", "password");
        userDAO.add(user);

        ProgressDAO progressDAO = DAOFactory.getInstance().getProgressDAO();

        List<Progress> progressesBeforeManipulations = progressDAO.getProgresses(theme);

        Progress progress = new Progress();
        progress.setUser(user);
        progress.setTheme(theme);
        progress.setProgress(75);

        progressDAO.add(progress);

        assertEquals(progressesBeforeManipulations.size() + 1, progressDAO.getProgresses(theme).size());
        assertEquals(progress, progressDAO.getProgress(user, theme));

        progressDAO.delete(progress);
        assertNull(progressDAO.getProgress(user, theme));
        userDAO.delete(user);
        assertNull(userDAO.getByID(user.getId()));
        themeDAO.delete(theme);
        assertNull(themeDAO.getByID(theme.getId()));
        sectionDAO.delete(section);
        assertNull(sectionDAO.getByID(section.getId()));

        assertEquals(progressesBeforeManipulations.size(), progressDAO.getProgresses(theme).size());
    }

    @Test
    public void test_MassDelete() {
        SectionDAO sectionDAO = DAOFactory.getInstance().getSectionDAO();
        Section section = new Section("JAVA");
        sectionDAO.add(section);

        UserDAO userDAO = DAOFactory.getInstance().getUserDAO();
        User user = new User("Bogdan", "Kobylinsky", "b@k.a", "password");
        userDAO.add(user);

        ThemeDAO themeDAO = DAOFactory.getInstance().getThemeDAO();

        ProgressDAO progressDAO = DAOFactory.getInstance().getProgressDAO();

        List<Progress> progressesBeforeDelete = progressDAO.getProgresses(user);

        List<Progress> progresses = new ArrayList<Progress>();
        for (int i = 0; i < 10; i++) {
            Theme theme = new Theme("Theme #" + i, "content content" + i, section);
            Progress progress = new Progress(user, theme, i * 10);
            progresses.add(progress);
        }

        for (Progress progress : progresses) {
            themeDAO.add(progress.getTheme());
            progressDAO.add(progress);
        }

        for (Progress progress : progresses) {
            progressDAO.delete(progress);
            themeDAO.delete(progress.getTheme());
        }

        List<Progress> progressesAfterDelete = progressDAO.getProgresses(user);

        assertTrue(progressesBeforeDelete.containsAll(progressesAfterDelete));
        assertTrue(progressesAfterDelete.containsAll(progressesBeforeDelete));

        userDAO.delete(user);
        sectionDAO.delete(section);
    }

    @Test
    public void test_AddUpdateDelete() {
        SectionDAO sectionDAO = DAOFactory.getInstance().getSectionDAO();
        Section section = new Section("JAVA");
        sectionDAO.add(section);

        ThemeDAO themeDAO = DAOFactory.getInstance().getThemeDAO();
        Theme theme = new Theme("Theme 1", "content content1", section);
        themeDAO.add(theme);

        UserDAO userDAO = DAOFactory.getInstance().getUserDAO();
        User user = new User("Bogdan", "Kobylinsky", "b@k.a", "password");
        userDAO.add(user);

        ProgressDAO progressDAO = DAOFactory.getInstance().getProgressDAO();
        Progress progress = new Progress();
        progress.setUser(user);
        progress.setTheme(theme);
        progress.setProgress(0);

        progressDAO.add(progress);
        assertEquals(progress, progressDAO.getProgress(user, theme));

        progress.setProgress(50);
        progressDAO.update(progress);
        assertEquals(progress, progressDAO.getProgress(user, theme));

        progress.setProgress(100);
        progressDAO.update(progress);
        assertEquals(progress, progressDAO.getProgress(user, theme));

        progressDAO.delete(progress);
        assertNull(progressDAO.getProgress(user, theme));
        userDAO.delete(user);
        assertNull(userDAO.getByID(user.getId()));
        themeDAO.delete(theme);
        assertNull(themeDAO.getByID(theme.getId()));
        sectionDAO.delete(section);
        assertNull(sectionDAO.getByID(section.getId()));
    }

}
