import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@Suite.SuiteClasses({
        UserTest.class,
        ThemeTest.class,
        ProgressTest.class,
        SectionTest.class,

        LoadTesting.class
})
@RunWith(Suite.class)
public class ComplexTestSuite {
}
