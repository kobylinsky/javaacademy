import com.bogdankobylinsky.javaacademy.dao.DAOFactory;
import com.bogdankobylinsky.javaacademy.dao.UserDAO;
import com.bogdankobylinsky.javaacademy.entities.User;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserTest {

    @Test
    public void test_AddGetDelete() {
        UserDAO userDAO = DAOFactory.getInstance().getUserDAO();

        List<User> usersBeforeManipulations = userDAO.getAll();

        User user = new User();
        user.setFirstName("Bogdan");
        user.setLastName("Kobylinsky");
        user.setLogin("b@k.a");
        user.setPassword("password");
        userDAO.add(user);

        assertEquals(usersBeforeManipulations.size() + 1, userDAO.getAll().size());
        assertEquals(user, userDAO.getByID(user.getId()));

        userDAO.delete(user);

        assertNull(userDAO.getByID(user.getId()));
        assertEquals(usersBeforeManipulations.size(), userDAO.getAll().size());
    }

    @Test
    public void test_MassDelete() {
        UserDAO userDAO = DAOFactory.getInstance().getUserDAO();

        List<User> usersBeforeDelete = userDAO.getAll();

        List<User> users = new ArrayList<User>();
        for (int i = 0; i < 10; i++)
            users.add(new User("John", "Doe", "john@com.ua", "password"));

        for (User user : users)
            userDAO.add(user);
        for (User user : users)
            userDAO.delete(user);

        List<User> usersAfterDelete = userDAO.getAll();

        assertTrue(usersBeforeDelete.containsAll(usersAfterDelete));
        assertTrue(usersAfterDelete.containsAll(usersBeforeDelete));
    }

    @Test
    public void test_AddUpdate() {
        UserDAO userDAO = DAOFactory.getInstance().getUserDAO();

        User user = new User("Bgdn", "Kblnsk", "b@k.a", "password");

        userDAO.add(user);
        Assert.assertEquals(user, userDAO.getByID(user.getId()));

        user.setFirstName("Bogdan");
        user.setLastName("Kobylinsky");
        user.setLogin("b@k.a");
        user.setPassword("password");
        userDAO.update(user);
        Assert.assertEquals(user, userDAO.getByID(user.getId()));

        userDAO.delete(user);
        assertNull(userDAO.getByID(user.getId()));
    }
}
