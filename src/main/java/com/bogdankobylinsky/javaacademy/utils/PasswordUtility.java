package com.bogdankobylinsky.javaacademy.utils;

import org.apache.commons.codec.binary.Hex;

import java.nio.charset.CharacterCodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordUtility {

    public static char[] getDigest(char[] password, String digestAlgorithm) {
        MessageDigest digester;
        char[] digest = null;
        try {
            byte[] inputBytes = PrimitiveArrayConverter.convertCharArrayToBytes(password, "UTF-8");

            digester = MessageDigest.getInstance(digestAlgorithm);
            digester.update(inputBytes);
            byte[] digestBytes = digester.digest();
            digest = Hex.encodeHex(digestBytes);
        } catch (NoSuchAlgorithmException | CharacterCodingException noAlgEx) {
            throw new RuntimeException(noAlgEx);
        }
        return digest;
    }
}
