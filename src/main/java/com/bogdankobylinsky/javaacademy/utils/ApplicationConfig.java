package com.bogdankobylinsky.javaacademy.utils;


import com.bogdankobylinsky.javaacademy.ws.JavaAcademyWS;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;

@ApplicationPath("/rest")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        return getRestResourceClasses();
    }

    private Set<Class<?>> getRestResourceClasses() {
        Set<Class<?>> resources = new java.util.HashSet<Class<?>>();
        resources.add(JavaAcademyWS.class);
        return resources;
    }
}
