package com.bogdankobylinsky.javaacademy.utils;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.Arrays;

public class PrimitiveArrayConverter {

    public static byte[] convertCharArrayToBytes(char[] input, String charsetName) throws CharacterCodingException {
        CharBuffer charBuffer = CharBuffer.wrap(input);

        Charset charSet = Charset.forName(charsetName);
        CharsetEncoder encoder = charSet.newEncoder();
        ByteBuffer bytes = encoder.encode(charBuffer);

        int limit = bytes.limit();
        byte[] bufferArray = bytes.array();
        byte[] result = Arrays.copyOf(bufferArray, limit);
        Arrays.fill(bufferArray, (byte) 0);

        return result;
    }

    public static char[] convertByteArrayToChars(byte[] input, String charsetName) throws CharacterCodingException {
        ByteBuffer buffer = ByteBuffer.wrap(input);

        Charset charset = Charset.forName(charsetName);
        CharsetDecoder decoder = charset.newDecoder();
        CharBuffer chars = decoder.decode(buffer);

        int limit = chars.limit();
        char[] bufferArray = chars.array();
        char[] result = Arrays.copyOf(bufferArray, limit);
        Arrays.fill(bufferArray, '0');

        return result;
    }
}
