package com.bogdankobylinsky.javaacademy.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "users")
@NamedQueries({
        @NamedQuery(name = "User.getAll", query = "SELECT u FROM User u"),
        @NamedQuery(name = "User.getByEmail", query = "SELECT u FROM User u WHERE u.email = :email")
})
public class User {

    @GeneratedValue
    private long id;

    @Id
    @Column(name = "EMAIL", length = 128)
    private String email;

    @Column(name = "FIRST_NAME", length = 128)
    private String firstName;

    @Column(name = "LAST_NAME", length = 128)
    private String lastName;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "PASSWORD", nullable = false, length = 128)
    private char[] password;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
    private List<Progress> progresses;

    @ElementCollection(targetClass = Group.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "users_groups",
            joinColumns = @JoinColumn(name = "EMAIL", nullable = false),
            uniqueConstraints = {@UniqueConstraint(columnNames = {"EMAIL", "GROUPNAME"})})
    @Enumerated(EnumType.STRING)
    @Column(name = "GROUPNAME", length = 64, nullable = false)
    private List<Group> groups;

    public User(String firstName, String lastName, String email, char[] password, Group group) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        groups = new ArrayList<>();
        groups.add(group);
    }

    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String login) {
        this.email = login;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void addGroup(Group group) {
        if (groups == null) {
            groups = new ArrayList<>();
        }
        groups.add(group);
    }

    public List<Progress> getProgresses() {
        return progresses;
    }

    public void setProgresses(List<Progress> progresses) {
        this.progresses = progresses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (!email.equals(user.email)) return false;
        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
        if (!groups.equals(user.groups)) return false;
        if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
        if (!Arrays.equals(password, user.password)) return false;
        if (progresses != null ? !progresses.equals(user.progresses) : user.progresses != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (email!= null ? email.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(password);
        result = 31 * result + (groups != null ? groups.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + Arrays.toString(password) + '\'' +
                ", groups=" + groups +
                '}';
    }
}
