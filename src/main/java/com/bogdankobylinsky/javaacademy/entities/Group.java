package com.bogdankobylinsky.javaacademy.entities;

public enum Group {
    ADMIN, USER
}