package com.bogdankobylinsky.javaacademy.entities;

import javax.persistence.*;

@Entity
@Table(name = "progresses")
@NamedQueries({
        @NamedQuery(name = "Progress.getAll",
                query = "SELECT p FROM Progress p"),
        @NamedQuery(name = "Progress.getByUser",
                query = "SELECT p FROM Progress p WHERE p.user = :user"),
        @NamedQuery(name = "Progress.getByTheme",
                query = "SELECT p FROM Progress p WHERE p.theme = :theme"),
        @NamedQuery(name = "Progress.getIndividual",
                query = "SELECT p FROM Progress p WHERE p.theme = :theme AND p.user = :user")
})
public class Progress {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID")
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "THEME_ID")
    private Theme theme;

    @Column(name = "PROGRESS")
    private int progress;

    public Progress(User user, Theme theme, int progress) {
        this.user = user;
        this.theme = theme;
        this.progress = progress;
    }

    public Progress() {
        user = new User();
        theme = new Theme();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Progress)) return false;

        Progress progress1 = (Progress) o;

        if (id != progress1.id) return false;
        if (progress != progress1.progress) return false;
        if (theme != null ? !theme.equals(progress1.theme) : progress1.theme != null) return false;
        if (user != null ? !user.equals(progress1.user) : progress1.user != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (theme != null ? theme.hashCode() : 0);
        result = 31 * result + progress;
        return result;
    }

    @Override
    public String toString() {
        return "Progress{" +
                "id=" + id +
                ", user=" + user +
                ", theme=" + theme +
                ", progress=" + progress +
                '}';
    }

    public enum Stage {
        NOT_STARTED, IN_PROGRESS, COMPLETED;
    }
}
