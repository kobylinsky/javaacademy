package com.bogdankobylinsky.javaacademy.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sections")
@NamedQueries({
        @NamedQuery(name = "Section.getAll",
                query = "SELECT s FROM Section s"),
        @NamedQuery(name = "Section.getByName",
                query = "SELECT s FROM Section s WHERE s.name LIKE :name")
})
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class Section implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlElement(required = true)
    private long id;

    @Column(name = "NAME", length = 300)
    @XmlElement(required = true)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "section")
    private List<Theme> themes;

    public Section(String name) {
        this();
        this.name = name;
    }

    public Section() {
        this.themes = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Theme> getThemes() {
        return themes;
    }

    public void setThemes(List<Theme> themes) {
        this.themes = themes;
    }

    public void addTheme(Theme theme) {
        themes.add(theme);
        if (theme.getSection() != this) {
            theme.setSection(this);
        }
    }

    public void deleteTheme(Theme theme) {
        themes.remove(theme);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Section)) return false;

        Section section = (Section) o;

        if (id != section.id) return false;
        if (!name.equals(section.name)) return false;
        if (!themes.equals(section.themes)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + name.hashCode();
        ;
        return result;
    }

    @Override
    public String toString() {
        return "Section{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", themes=" + themes.size() +
                '}';
    }
}
