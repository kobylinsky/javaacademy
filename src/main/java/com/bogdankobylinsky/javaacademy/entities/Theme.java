package com.bogdankobylinsky.javaacademy.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "themes")
@NamedQueries({
        @NamedQuery(name = "Theme.getAll",
                query = "SELECT t FROM Theme t"),
        @NamedQuery(name = "Theme.getBySection",
                query = "SELECT t FROM Theme t WHERE t.section = :section")
})
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class Theme implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlElement(required=true)
    private long id;

    @Column(name = "NAME", length = 300)
    @XmlElement(required=true)
    private String name;

    @Column(name = "CONTENT", length = 50000)
    @XmlElement(required=true)
    private String content;

    @ManyToOne
    @JoinColumn(name = "SECTION")
    @XmlElement(required=true)
    private Section section;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "theme", fetch = FetchType.LAZY)
    private List<Progress> progresses;

    public Theme(String name, String content, Section section) {
        setName(name);
        setContent(content);
        setSection(section);
    }

    public Theme() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        if (section == null && this.section != null)
            this.section.deleteTheme(this);
        if (section != null && !section.getThemes().contains(this))
            section.addTheme(this);

        this.section = section;
    }

    public List<Progress> getProgresses() {
        return progresses;
    }

    public void setProgresses(List<Progress> progresses) {
        this.progresses = progresses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Theme)) return false;

        Theme theme = (Theme) o;

        if (id != theme.id) return false;
        if (!content.equals(theme.content)) return false;
        if (!name.equals(theme.name)) return false;
        if (section != null ? !section.equals(theme.section) : theme.section != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (section != null ? section.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Theme{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", section=" + section +
                '}';
    }
}
