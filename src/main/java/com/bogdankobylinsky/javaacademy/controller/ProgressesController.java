package com.bogdankobylinsky.javaacademy.controller;

import com.bogdankobylinsky.javaacademy.ejb.ProgressService;
import com.bogdankobylinsky.javaacademy.entities.Progress;
import com.bogdankobylinsky.javaacademy.entities.Theme;
import com.bogdankobylinsky.javaacademy.entities.User;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

@ManagedBean(name = "progressesController")
@SessionScoped
public class ProgressesController {

    @EJB
    private ProgressService progressService;

    private Progress progress;
    private DataModel progresses;

    public ProgressesController() {
    }

    @PostConstruct
    private void init() {
        progresses = new ListDataModel();
        initFields();
    }

    public void startProgress() {
        progressService.add(getProgressOfCurrentUser());
        initFields();
    }

    public void completeProgress() {
        //Progress rowData = (Progress) progresses.getRowData();
        Progress progr = getProgressOfCurrentUser();
        progr.setProgress(100);
        progressService.update(progr);
        initFields();
    }

    public void deleteProgress() {
        Progress rowData = (Progress) progresses.getRowData();
        progressService.delete(rowData);
        initFields();
    }

    private void initFields() {
        progresses.setWrappedData(progressService.getAll());
        progress = new Progress();
    }

    public Progress getProgress() {
        return progress;
    }

    public void setProgress(Progress progress) {
        this.progress = progress;
    }

    public DataModel getProgresses() {
        return progresses;
    }

    public void setProgresses(ListDataModel progresses) {
        this.progresses = progresses;
    }

    public DataModel getProgresses(Theme theme) {
        progresses.setWrappedData(progressService.getProgresses(theme));
        return progresses;
    }

    private Progress.Stage getProgressStage(User user, Theme theme) {
        Progress progressOfUser = progressService.getProgress(user, theme);
        if (progressOfUser != null && progressOfUser.getProgress() == 100)
            return Progress.Stage.COMPLETED;
        if (progressOfUser != null && progressOfUser.getProgress() < 100)
            return Progress.Stage.IN_PROGRESS;
        return Progress.Stage.NOT_STARTED;
    }

    public boolean isProgressNotStarted(User user, Theme theme) {
        return getProgressStage(user, theme) == Progress.Stage.NOT_STARTED;
    }

    public boolean isProgressInProgress(User user, Theme theme) {
        return getProgressStage(user, theme) == Progress.Stage.IN_PROGRESS;
    }

    public boolean isProgressCompleted(User user, Theme theme) {
        return getProgressStage(user, theme) == Progress.Stage.COMPLETED;
    }

    private Progress getProgressOfCurrentUser() {
        ThemesController themesController = (ThemesController) FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().get("themesController");
        Theme theme = themesController.getTheme();
        progress.setTheme(theme);

        UsersController usersController = (UsersController) FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().get("usersController");
        User user = usersController.getUser();
        progress.setUser(user);

        progress = progressService.getProgress(user, theme);
        
        if (progress == null)
            progress = new Progress(user, theme, 0);

        return progress;
    }

}
