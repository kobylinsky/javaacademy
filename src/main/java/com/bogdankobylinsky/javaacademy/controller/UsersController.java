package com.bogdankobylinsky.javaacademy.controller;

import com.bogdankobylinsky.javaacademy.ejb.UserService;
import com.bogdankobylinsky.javaacademy.entities.Group;
import com.bogdankobylinsky.javaacademy.entities.User;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

@ManagedBean(name = "usersController")
@SessionScoped
public class UsersController {

    @EJB
    private UserService userService;

    private User user;
    private DataModel users;

    public UsersController() {
    }

    @PostConstruct
    private void init() {
        users = new ListDataModel();
        initFields();
    }

    public String saveUser() {
        user.addGroup(Group.USER);
        userService.add(user);
        initFields();
        return "index";
    }

    public String removeUser() {
        userService.delete((User) users.getRowData());
        initFields();
        return "listUsers";
    }

    public String editUser() {
        userService.update(user);
        initFields();
        return "index";
    }

    public String prepareEditUser() {
        user = (User) users.getRowData();
        return "editUser";
    }

    public String prepareEditCurrentUser() {
        user = userService.getByEmail(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
        return "editUser";
    }

    private void initFields() {
        users.setWrappedData(userService.getAll());
        user = new User();
    }

    public User getUser() {
        if (user == null)
            user = userService.getByEmail(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DataModel getUsers() {
        return users;
    }

    public void setUsers(ListDataModel users) {
        this.users = users;
    }

}
