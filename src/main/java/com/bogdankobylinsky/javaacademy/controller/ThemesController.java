package com.bogdankobylinsky.javaacademy.controller;

import com.bogdankobylinsky.javaacademy.ejb.SectionService;
import com.bogdankobylinsky.javaacademy.ejb.ThemeService;
import com.bogdankobylinsky.javaacademy.entities.Section;
import com.bogdankobylinsky.javaacademy.entities.Theme;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

@ManagedBean(name = "themesController")
@SessionScoped
public class ThemesController {

    @EJB
    private ThemeService themeService;
    @EJB
    private SectionService sectionService;

    private DataModel sections;
    private Section section;

    private DataModel themes;
    private String themesTitle;

    private Theme theme;
    private String themeTitle;


    public ThemesController() {
    }

    @PostConstruct
    private void init() {
        sections = new ListDataModel();
        themes = new ListDataModel();
        initFields();
    }

    //
    // Operations under Section
    //
    public String saveSection() {
        sectionService.add(section);
        initFields();
        return "listSections";
    }

    public String removeSection() {
        sectionService.delete(section);
        initFields();
        return "listSections";
    }

    public String editSection() {
        sectionService.update(section);
        initFields();
        return "listSections";
    }

    public String prepareEditSection() {
        return "editSection";
    }

    public String prepareAddSection() {
        section = new Section();
        return "addSection.xhtml";
    }

    //
    // Operations under Theme
    //
    public String saveTheme() {
        themeService.add(theme);
        initFields();
        return "listSections";
    }

    public String removeTheme() {
        themeService.delete(theme);
        initFields();
        return "listSections";
    }

    public String editTheme() {
        themeService.update(theme);
        initFields();
        return "listSections";
    }

    public String prepareEditTheme() {
        theme.setSection(new Section());
        return "editTheme";
    }

    public String prepareAddTheme() {
        theme = new Theme();
        theme.setSection(new Section());
        return "addTheme.xhtml";
    }

    private void initFields() {
        sections.setWrappedData(sectionService.getAll());
        themes.setWrappedData(themeService.getAll());
        section = new Section();
        theme = new Theme();
    }

    public DataModel getSections() {
        sections.setWrappedData(sectionService.getAll());
        return sections;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public Section getSection() {
        return section;
    }

    public void setSections(DataModel sections) {
        this.sections = sections;
    }

    public DataModel getThemes() {
        themes = new ListDataModel<>();
        String sectionId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("sectionid");
        if (sectionId != null) {
            section = sectionService.getByID(Integer.parseInt(sectionId));
            themes.setWrappedData(themeService.getBySection(section));
        } else {
            themes.setWrappedData(themeService.getAll());
        }
        return themes;
    }

    public void setThemes(DataModel themes) {
        this.themes = themes;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public String getThemesTitle() {
        String sectionId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
                .get("sectionid");
        if (sectionId != null) {
            int sectId = Integer.parseInt(sectionId);
            section = sectionService.getByID(sectId);
            if (section != null) {
                return "Themes in section \"" + section.getName() + "\"";
            }
        }
        section = null;
        return "All themes in all sections";
    }

    public String initTheme() {
        String themeId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
                .get("id");
        if (themeId != null) {
            int tId = Integer.parseInt(themeId);
            theme = themeService.getByID(tId);
            if (theme != null) {
                section = theme.getSection();
                return theme.getName();
            }
        } else if (theme != null && theme.getName() != null) {
            section = theme.getSection();
            return theme.getName();
        }
        return "Oops...";
    }

}
