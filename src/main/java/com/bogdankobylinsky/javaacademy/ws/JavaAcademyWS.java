package com.bogdankobylinsky.javaacademy.ws;

import com.bogdankobylinsky.javaacademy.dao.impl.jpa.SectionJPADAO;
import com.bogdankobylinsky.javaacademy.dao.impl.jpa.ThemeJPADAO;
import com.bogdankobylinsky.javaacademy.entities.Theme;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

@Stateless
@Path("/services")
public class JavaAcademyWS {

    @EJB
    private ThemeJPADAO themeJPADAO;

    @EJB
    private SectionJPADAO sectionJPADAO;

    @GET
    @Path("/sections/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public String getSection(@PathParam("id") long id) {
        return convertToXml(sectionJPADAO.getByID(id));
    }


    @GET
    @Path("/themes/{id}")
    @Produces(MediaType.TEXT_XML)
    public String getTheme(@PathParam("id") long id) {
        return convertToXml(themeJPADAO.getByID(id));
    }


    @GET
    @Produces(MediaType.TEXT_XML)
    public String sayXMLHello() {
        return "<?xml version=\"1.0\"?>" +
                "<hello>" +
                "WELCOME TO SERVICES." +
                "</hello>";
    }


    private String convertToXml(Object object) {
        StringWriter sw = new StringWriter();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Theme.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(object, sw);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return sw.toString();
    }
}
