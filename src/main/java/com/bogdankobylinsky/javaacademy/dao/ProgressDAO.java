package com.bogdankobylinsky.javaacademy.dao;

import com.bogdankobylinsky.javaacademy.entities.Progress;
import com.bogdankobylinsky.javaacademy.entities.Theme;
import com.bogdankobylinsky.javaacademy.entities.User;

import java.util.List;

public interface ProgressDAO extends GenericDAO<Progress> {

    List<Progress> getProgresses(User user);

    List<Progress> getProgresses(Theme theme);

    Progress getProgress(User user, Theme theme);
}
