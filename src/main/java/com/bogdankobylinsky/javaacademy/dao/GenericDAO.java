package com.bogdankobylinsky.javaacademy.dao;

import java.util.List;

public interface GenericDAO<T> {
    List<T> getAll();

    T getByID(long id);

    void add(T object);

    void update(T object);

    void delete(T object);
}
