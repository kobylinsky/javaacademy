package com.bogdankobylinsky.javaacademy.dao;

import com.bogdankobylinsky.javaacademy.entities.Section;
import com.bogdankobylinsky.javaacademy.entities.Theme;

import java.util.List;

public interface ThemeDAO extends GenericDAO<Theme> {

    List<Theme> getBySection(Section section);

}
