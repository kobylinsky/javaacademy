package com.bogdankobylinsky.javaacademy.dao;

import com.bogdankobylinsky.javaacademy.entities.User;

public interface UserDAO extends GenericDAO<User> {

    User getByEmail(String email);
}
