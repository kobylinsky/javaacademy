package com.bogdankobylinsky.javaacademy.dao.impl.jpa;

import com.bogdankobylinsky.javaacademy.dao.UserDAO;
import com.bogdankobylinsky.javaacademy.entities.User;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
@LocalBean
public class UserJPADAO implements UserDAO {

    @PersistenceContext
    private EntityManager em;

    public UserJPADAO() {
    }

    @Override
    public List<User> getAll() {
        return em.createNamedQuery("User.getAll").getResultList();
    }

    @Override
    public User getByID(long id) {
        return em.find(User.class, id);
    }

    @Override
    public void delete(User user) {
        User userForRemove = em.merge(user);
        em.remove(userForRemove);
    }

    @Override
    public void update(User user) {
        em.merge(user);
    }

    @Override
    public void add(User user) {
        em.persist(user);
    }

    @Override
    public User getByEmail(String email) {
        Query namedQuery = em.createNamedQuery("User.getByEmail").setParameter("email", email);
        User user = null;
        try {
            user = (User) namedQuery.getSingleResult();
        } catch (NoResultException ignored) {
        }

        return user;
    }
}
