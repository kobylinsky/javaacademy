package com.bogdankobylinsky.javaacademy.dao.impl.jpa;

import com.bogdankobylinsky.javaacademy.dao.ProgressDAO;
import com.bogdankobylinsky.javaacademy.entities.Progress;
import com.bogdankobylinsky.javaacademy.entities.Theme;
import com.bogdankobylinsky.javaacademy.entities.User;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.List;

@Stateless
@LocalBean
public class ProgressJPADAO implements ProgressDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Progress> getAll() {
        Query namedQuery = em.createNamedQuery("Progress.getAll");
        return namedQuery.getResultList();
    }

    @Override
    public Progress getByID(long id) {
        return em.find(Progress.class, id);
    }

    @Override
    public void delete(Progress progress) {
        Progress progressForRemove = em.merge(progress);
        em.remove(progressForRemove);
    }

    @Override
    public void update(Progress progress) {
        em.merge(progress);
    }

    @Override
    public void add(Progress progress) {
        em.persist(progress);
    }

    @Override
    public List<Progress> getProgresses(User user) {
        return em.createNamedQuery("Progress.getByUser").setParameter("user", user).getResultList();
    }

    @Override
    public List<Progress> getProgresses(Theme theme) {
        return em.createNamedQuery("Progress.getByTheme").setParameter("theme", theme).getResultList();
    }

    @Override
    public Progress getProgress(User user, Theme theme) {
        Query namedQuery = em.createNamedQuery("Progress.getIndividual")
                .setParameter("theme", theme)
                .setParameter("user", user);
        Progress progress = null;
        try {
            progress = (Progress) namedQuery.getSingleResult();
        } catch (NoResultException ignored) {
        }

        return progress;
    }

}