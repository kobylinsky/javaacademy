package com.bogdankobylinsky.javaacademy.dao.impl.jpa;

import com.bogdankobylinsky.javaacademy.dao.SectionDAO;
import com.bogdankobylinsky.javaacademy.entities.Section;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
@LocalBean
public class SectionJPADAO implements SectionDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Section> getAll() {
        return em.createNamedQuery("Section.getAll").getResultList();
    }

    @Override
    public Section getByID(long id) {
        return em.find(Section.class, id);
    }

    @Override
    public Section getByName(String name) {
        Query namedQuery = em.createNamedQuery("Section.getByName").setParameter("name", name);
        Section section = null;
        try {
            section = (Section) namedQuery.getSingleResult();
        } catch (NoResultException ignored) {
        }

        return section;
    }

    @Override
    public void delete(Section section) {
        Section sectionForRemove = em.merge(section);
        em.remove(sectionForRemove);
    }

    @Override
    public void update(Section section) {
        em.merge(section);
    }

    @Override
    public void add(Section section) {
        em.persist(section);
    }

}
