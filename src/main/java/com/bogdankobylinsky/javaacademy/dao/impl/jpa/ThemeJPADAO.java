package com.bogdankobylinsky.javaacademy.dao.impl.jpa;

import com.bogdankobylinsky.javaacademy.dao.ThemeDAO;
import com.bogdankobylinsky.javaacademy.entities.Section;
import com.bogdankobylinsky.javaacademy.entities.Theme;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@LocalBean
public class ThemeJPADAO implements ThemeDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Theme> getAll() {
        return em.createNamedQuery("Theme.getAll").getResultList();
    }

    @Override
    public Theme getByID(long id) {
        return em.find(Theme.class, id);
    }

    @Override
    public List<Theme> getBySection(Section section) {
        return em.createNamedQuery("Theme.getBySection").setParameter("section", section).getResultList();
    }

    @Override
    public void delete(Theme theme) {
        Section themesSection = theme.getSection();
        if (themesSection != null)
            em.merge(themesSection);
        Theme themeForRemove = em.merge(theme);
        em.remove(themeForRemove);
    }

    @Override
    public void update(Theme theme) {
        em.merge(theme);
    }

    @Override
    public void add(Theme theme) {
        em.persist(theme);
    }
}
