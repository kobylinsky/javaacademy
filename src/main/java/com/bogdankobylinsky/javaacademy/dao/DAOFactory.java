package com.bogdankobylinsky.javaacademy.dao;

import com.bogdankobylinsky.javaacademy.dao.impl.jpa.ProgressJPADAO;
import com.bogdankobylinsky.javaacademy.dao.impl.jpa.SectionJPADAO;
import com.bogdankobylinsky.javaacademy.dao.impl.jpa.ThemeJPADAO;
import com.bogdankobylinsky.javaacademy.dao.impl.jpa.UserJPADAO;

public class DAOFactory {
    
    private static DAOFactory instance = null;
    private static final DBType DB_TYPE = DBType.JPA;

    public static synchronized DAOFactory getInstance() {
        if (instance == null) {
            instance = new DAOFactory();
        }
        return instance;
    }


    public ThemeDAO getThemeDAO() {
        switch (DB_TYPE) {
            case JPA: return new ThemeJPADAO();
        }
        return null;
    }

    public UserDAO getUserDAO() {
        switch (DB_TYPE) {
            case JPA: return new UserJPADAO();
        }
        return null;
    }

    public ProgressDAO getProgressDAO() {
        switch (DB_TYPE) {
            case JPA: return new ProgressJPADAO();
        }
        return null;
    }

    public SectionDAO getSectionDAO() {
        switch (DB_TYPE) {
            case JPA: return new SectionJPADAO();
        }
        return null;
    }

    public enum DBType {
        @Deprecated MONGO,
        @Deprecated JDBC,
        JPA
    }

}
