package com.bogdankobylinsky.javaacademy.dao;

import com.bogdankobylinsky.javaacademy.entities.Section;

public interface SectionDAO extends GenericDAO<Section> {

    Section getByName(String name);
}
