package com.bogdankobylinsky.javaacademy.ejb;

import com.bogdankobylinsky.javaacademy.dao.impl.jpa.ThemeJPADAO;
import com.bogdankobylinsky.javaacademy.entities.Section;
import com.bogdankobylinsky.javaacademy.entities.Theme;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@EJB(name = "java:javaacademy/ThemeService", beanInterface = ThemeService.class)
public class ThemeServiceImpl implements ThemeService {

    @EJB
    private ThemeJPADAO themeDAO;

    @Override
    public List<Theme> getBySection(Section section) {
        return themeDAO.getBySection(section);
    }

    @Override
    public List<Theme> getAll() {
        return themeDAO.getAll();
    }

    @Override
    public Theme getByID(long id) {
        return themeDAO.getByID(id);
    }

    @Override
    @RolesAllowed({"ADMIN"})
    public void delete(Theme theme) {
        themeDAO.delete(theme);
    }

    @Override
    @RolesAllowed({"ADMIN"})
    public void update(Theme theme) {
        themeDAO.update(theme);
    }

    @Override
    @RolesAllowed({"ADMIN"})
    public void add(Theme theme) {
        themeDAO.add(theme);
    }
}
