package com.bogdankobylinsky.javaacademy.ejb;

import com.bogdankobylinsky.javaacademy.dao.impl.jpa.SectionJPADAO;
import com.bogdankobylinsky.javaacademy.entities.Section;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@EJB(name = "java:javaacademy/SectionService", beanInterface = SectionService.class)
public class SectionServiceImpl implements SectionService {

    @EJB
    private SectionJPADAO sectionDAO;

    @Override
    public List<Section> getAll() {
        return sectionDAO.getAll();
    }

    @Override
    public Section getByID(long id) {
        return sectionDAO.getByID(id);
    }

    @Override
    @RolesAllowed({"ADMIN"})
    public void delete(Section section) {
        sectionDAO.delete(section);
    }

    @Override
    @RolesAllowed({"ADMIN"})
    public void update(Section section) {
        sectionDAO.update(section);
    }

    @Override
    @RolesAllowed({"ADMIN"})
    public void add(Section section) {
        sectionDAO.add(section);
    }

    @Override
    public Section getByName(String name) {
        return sectionDAO.getByName(name);
    }
}
