package com.bogdankobylinsky.javaacademy.ejb;

import com.bogdankobylinsky.javaacademy.dao.impl.jpa.UserJPADAO;
import com.bogdankobylinsky.javaacademy.entities.User;
import com.bogdankobylinsky.javaacademy.utils.PasswordUtility;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@EJB(name = "java:javaacademy/UserService", beanInterface = UserService.class)
public class UserServiceImpl implements UserService {

    @EJB
    private UserJPADAO userDAO;

    @Override
    public List<User> getAll() {
        return userDAO.getAll();
    }

    @Override
    public User getByID(long id) {
        return userDAO.getByID(id);
    }

    @Override
    @RolesAllowed({"ADMIN"})
    public void delete(User user) {
        userDAO.delete(user);
    }

    @Override
    public void update(User user) {
        userDAO.update(user);
    }

    @Override
    public void add(User user) {
        char[] digest = PasswordUtility.getDigest(user.getPassword(), "MD5");
        user.setPassword(digest);
        userDAO.add(user);
    }

    @Override
    public User getByEmail(String email) {
        return userDAO.getByEmail(email);
    }
}
