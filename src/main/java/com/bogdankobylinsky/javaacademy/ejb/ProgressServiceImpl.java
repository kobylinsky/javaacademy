package com.bogdankobylinsky.javaacademy.ejb;

import com.bogdankobylinsky.javaacademy.controller.ThemesController;
import com.bogdankobylinsky.javaacademy.controller.UsersController;
import com.bogdankobylinsky.javaacademy.dao.impl.jpa.ProgressJPADAO;
import com.bogdankobylinsky.javaacademy.entities.Progress;
import com.bogdankobylinsky.javaacademy.entities.Theme;
import com.bogdankobylinsky.javaacademy.entities.User;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import java.util.List;

@Stateless
@EJB(name = "java:javaacademy/ProgressService", beanInterface = ProgressService.class)
public class ProgressServiceImpl implements ProgressService {

    @EJB
    private ProgressJPADAO progressDAO;

    @Override
    public List<Progress> getProgresses(User user) {
        return progressDAO.getProgresses(user);
    }

    @Override
    public List<Progress> getProgresses(Theme theme) {
        return progressDAO.getProgresses(theme);
    }

    @Override
    public Progress getProgress(User user, Theme theme) {
        return progressDAO.getProgress(user, theme);
    }

    @Override
    public List<Progress> getAll() {
        return progressDAO.getAll();
    }

    @Override
    public Progress getByID(long id) {
        return progressDAO.getByID(id);
    }

    @Override
    @RolesAllowed({"ADMIN"})
    public void delete(Progress progress) {
        progressDAO.delete(progress);
    }

    @Override
    public void update(Progress progress) {
        progressDAO.update(progress);
    }

    @Override
    public void add(Progress progress) {
        progress.setProgress(0);

        progressDAO.add(progress);
    }
}
